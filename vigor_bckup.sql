-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: vigor
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `desk`
--

DROP TABLE IF EXISTS `desk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `desk` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  `number_of_people` int NOT NULL,
  `status` bit(1) NOT NULL,
  `status_name` varchar(255) NOT NULL,
  `img_url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_s3vyudamacn9nl3pdkjn5l0v9` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `desk`
--

LOCK TABLES `desk` WRITE;
/*!40000 ALTER TABLE `desk` DISABLE KEYS */;
INSERT INTO `desk` VALUES (1,'D001','2021-11-16 23:55:11.610000',NULL,2,_binary '','ready',''),(2,'D002','2021-11-16 23:55:36.988000',NULL,2,_binary '','ready',''),(3,'D003','2021-11-17 01:14:17.132000',NULL,3,_binary '\0','maintained',''),(25,'D004','2021-11-22 11:17:50.660000',NULL,3,_binary '\0','booking','');
/*!40000 ALTER TABLE `desk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packet`
--

DROP TABLE IF EXISTS `packet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `packet` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  `description` text,
  `img_url` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packet`
--

LOCK TABLES `packet` WRITE;
/*!40000 ALTER TABLE `packet` DISABLE KEYS */;
INSERT INTO `packet` VALUES (19,'2021-11-25 00:20:07.822000',NULL,'Paket shabu Grill kamu hanya tinggal grill, sudah lengkap untuk di santap bareng, paket terdiri dari :\n\nBeef Marinasi Bulgogi/ gochujang 500gr\nChicken Marinasi gochujang/bulgogi 500gr\nCocktail beef Sosis 2 pc\nSauce cocol 60ml\nSelada Fresh 150gram\nPackaging tinwall pack','http://localhost:8093/images/packet-2021-11-25-000.jpeg','BBQ/Grill',150000),(20,'2021-11-25 00:35:07.877000',NULL,'Paket terdiri :\nBeef Suki 100gr\nFish ball 350gr\nslice chicken 100gr\nSawi putih 100gr\nPackcoy 100gr\nJagung 2 potong\nMie kriting 2pc\nJamur Enoki 100gr\nBumbu tomyam 150ml','http://localhost:8093/images/packet-2021-11-25-001.jpg','Shabu Tomyum',120000),(21,'2021-11-25 00:38:46.327000',NULL,'Lengkap BBQ, praktis dan mudah pastinya dengan harga lebih murah, BBQ grill pan/bulgogi.','http://localhost:8093/images/packet-2021-11-25-002.jpg','Grill Bulgogi',130000),(22,'2021-11-25 00:40:59.800000',NULL,'Terdiri dari :\nBeef Suki 250gr\nFish ball 1000gr\nCumi flower 200gr\nSawi putih 100gr\nPackcoy 100gr\nJagung 2 potong\nMie kriting 2pc\nJamur Enoki 100gr\nBumbu tomyam 150ml\nSeledri penambah cita rasa','http://localhost:8093/images/packet-2021-11-25-003.jpeg','Shabu Kuah Tomyam',140000),(23,'2021-11-25 00:42:33.555000',NULL,'Wagyu Saikoro Premium','http://localhost:8093/images/packet-2021-11-25-004.jpg','Wagyu Saikoro Premium',160000),(24,'2021-11-25 00:44:25.692000',NULL,'Paket terdiri :\nBeef Suki 200gr\nFish ball 500gr\nCumi flower 100gr','http://localhost:8093/images/packet-2021-11-25-005.jpg','Grill Premium',100000),(25,'2021-11-25 00:45:54.581000',NULL,'Paket terdiri :\nBeef Suki 250gr\nFish ball 1000gr\nCumi flower 200gr','http://localhost:8093/images/packet-2021-11-25-006.jpg','Grill Combo',150000),(26,'2021-11-25 00:48:24.700000',NULL,'Paket terdiri :\nBeef Suki 250gr\nFish ball 1000gr\nCumi flower 200gr\nSawi putih 100gr\nPackcoy 100gr\nJagung 2 potong\nMie kriting 2pc\nJamur Enoki 100gr\nBumbu tomyam 150ml\nSeledri penambah cita rasa','http://localhost:8093/images/packet-2021-11-25-007.jpg','Complete Grill',200000);
/*!40000 ALTER TABLE `packet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'2021-11-16 23:40:42.942000',NULL,'ROLE_USER'),(2,'2021-11-16 23:40:53.781000',NULL,'ROLE_ADMIN'),(3,'2021-11-16 23:42:00.858000',NULL,'ROLE_WAITER'),(4,'2021-11-16 23:42:09.688000',NULL,'ROLE_CASHIER');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipment`
--

DROP TABLE IF EXISTS `shipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shipment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `shipment_status_id` int DEFAULT NULL,
  `transaction_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6jlpponlpel36cd0slfnl1ydy` (`shipment_status_id`),
  KEY `FK4vkjac97t9nsq2ve8hm4i0tx8` (`transaction_id`),
  KEY `FKsh7hhx6dbueu6ilnf92s9jub9` (`user_id`),
  CONSTRAINT `FK4vkjac97t9nsq2ve8hm4i0tx8` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`id`),
  CONSTRAINT `FK6jlpponlpel36cd0slfnl1ydy` FOREIGN KEY (`shipment_status_id`) REFERENCES `shipment_status` (`id`),
  CONSTRAINT `FKsh7hhx6dbueu6ilnf92s9jub9` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipment`
--

LOCK TABLES `shipment` WRITE;
/*!40000 ALTER TABLE `shipment` DISABLE KEYS */;
/*!40000 ALTER TABLE `shipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipment_status`
--

DROP TABLE IF EXISTS `shipment_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shipment_status` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipment_status`
--

LOCK TABLES `shipment_status` WRITE;
/*!40000 ALTER TABLE `shipment_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `shipment_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `token`
--

DROP TABLE IF EXISTS `token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `token` (
  `id` int NOT NULL AUTO_INCREMENT,
  `jwt_token` varchar(255) DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKe32ek7ixanakfqsdaokm4q9y2` (`user_id`),
  CONSTRAINT `FKe32ek7ixanakfqsdaokm4q9y2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token`
--

LOCK TABLES `token` WRITE;
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
/*!40000 ALTER TABLE `token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) DEFAULT NULL,
  `grand_total` int NOT NULL,
  `invoice_url` varchar(255) DEFAULT NULL,
  `jam_booking` varchar(255) DEFAULT NULL,
  `leftover` float NOT NULL,
  `overtime` float NOT NULL,
  `status` bit(1) NOT NULL,
  `tanggal_booking` varchar(255) DEFAULT NULL,
  `total_penalty` float NOT NULL,
  `desk_id` int DEFAULT NULL,
  `transaction_type_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `payment_token` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKkw578kw2mdg1ygghm3alfo7f4` (`desk_id`),
  KEY `FKnl0vpl01y6vu03hkpi4xupugo` (`transaction_type_id`),
  KEY `FKsg7jp0aj6qipr50856wf6vbw1` (`user_id`),
  CONSTRAINT `FKkw578kw2mdg1ygghm3alfo7f4` FOREIGN KEY (`desk_id`) REFERENCES `desk` (`id`),
  CONSTRAINT `FKnl0vpl01y6vu03hkpi4xupugo` FOREIGN KEY (`transaction_type_id`) REFERENCES `transaction_type` (`id`),
  CONSTRAINT `FKsg7jp0aj6qipr50856wf6vbw1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_detail`
--

DROP TABLE IF EXISTS `transaction_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction_detail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) DEFAULT NULL,
  `qty` int NOT NULL,
  `packet_id` int DEFAULT NULL,
  `transaction_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKs5vivl68kreexkl7ughyx4fk1` (`packet_id`),
  KEY `FK2nh7hmi2mfurimsk0viq4a127` (`transaction_id`),
  CONSTRAINT `FK2nh7hmi2mfurimsk0viq4a127` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`id`),
  CONSTRAINT `FKs5vivl68kreexkl7ughyx4fk1` FOREIGN KEY (`packet_id`) REFERENCES `packet` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=312 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_detail`
--

LOCK TABLES `transaction_detail` WRITE;
/*!40000 ALTER TABLE `transaction_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_type`
--

DROP TABLE IF EXISTS `transaction_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_type`
--

LOCK TABLES `transaction_type` WRITE;
/*!40000 ALTER TABLE `transaction_type` DISABLE KEYS */;
INSERT INTO `transaction_type` VALUES (1,'2021-11-16 23:53:45.104000',NULL,'online'),(2,'2021-11-16 23:53:51.776000',NULL,'offline'),(3,'2021-11-16 23:53:57.855000',NULL,'delivery');
/*!40000 ALTER TABLE `transaction_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `address` varchar(255) NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `role_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ob8kqyqqgmefl0aco34akdtpe` (`email`),
  KEY `FKn82ha3ccdebhokx3a8fgdqeyy` (`role_id`),
  CONSTRAINT `FKn82ha3ccdebhokx3a8fgdqeyy` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Jl.Admin','2021-11-16 23:38:18.542000',NULL,'admin@gmail.com','Admin','$2a$10$IsbmJs7gL5JnS1Xr0Cr3I.rZH3Qf7uigs0w.UCoVjhVAEfVhSWdn6','89666545555',2),(2,'Jl.User','2021-11-16 23:44:13.835000',NULL,'user@gmail.com','User','$2a$10$5gLAC4BNbgyT38EXTB4yHOWIgRD7iiXchepduZXMfkphfbQ7c9Yma','89666545555',1),(3,'Jl.Waiter','2021-11-17 09:19:50.959000',NULL,'waiter@gmail.com','Waiter','$2a$10$VUouXNNZ0XS8eZxxAYQEEO2iAqccsGfElPh5U4dPHUvBpcNNG77t2','89666545555',3),(4,'Jl. Cashier','2021-11-18 13:07:38.700000',NULL,'cashier@gmail.com','Cashier','$2a$10$Qqva/.vhGAFEw9pg6MGwnev6fAZbaIQ70o5lb1guVLxdznUJX5WcO','89776654567',4);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-25  1:20:54
