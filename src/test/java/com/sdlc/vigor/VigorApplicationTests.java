package com.sdlc.vigor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.sdlc.vigor.controller.DeskController;
import com.sdlc.vigor.controller.RoleController;
import com.sdlc.vigor.entity.Desk;
import com.sdlc.vigor.entity.Role;
import com.sdlc.vigor.repository.DeskRepository;
import com.sdlc.vigor.repository.RoleRepository;
import com.sdlc.vigor.response.ResponseListRole;
import com.sdlc.vigor.response.ResponseObject;
import com.sdlc.vigor.service.DeskService;
import com.sdlc.vigor.service.RoleService;

@SpringBootTest
@AutoConfigureMockMvc
class VigorApplicationTests {
	
	@InjectMocks
    RoleController roleController;
	@Mock
    RoleService roleService;
	@InjectMocks
    RoleService roleServiceInject;
	@Mock
    RoleRepository roleRepo;
    List<Role> roles;
	
	@InjectMocks
    DeskController deskController;
	@Mock
    DeskService deskService;
	@InjectMocks
    DeskService deskServiceInject;
	@Mock
    DeskRepository deskRepo;
    List<Desk> desks;
    
    @Autowired
	private MockMvc mockMvc;
    
    @BeforeEach
    void init() {
    	MockitoAnnotations.openMocks(this);
    	desks = new ArrayList<Desk>();
    	roles = new ArrayList<Role>();
    }
    
    @Test
	void contextLoads() {
    	assertThat(roleController).isNotNull();
    	assertThat(deskController).isNotNull();
	}
    
    @Test
    @WithMockUser(roles = "ADMIN")
	void testShouldReturnAllRoles() throws Exception {
    	lenient().when(roleRepo.findAll()).thenReturn(roles);
    	MockHttpServletResponse res = mockMvc.perform(get("/api/roles")).andReturn().getResponse();
    	System.out.println(res.getContentAsString());
    	assertTrue(res.getContentAsString().contains("ROLE_ADMIN"));
	}
    
    @Test
	void testAddRole() {
    	Role newRole = new Role();
		lenient().when(roleService.addRole(newRole)).thenReturn(ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject(HttpStatus.OK.value(), "Add Role Success", newRole)));
		assertEquals(newRole, roleController.addRole(newRole).getBody().getData());
	}
    
    @Test
	void testFindById() {
    	Role newRole = new Role();
		lenient().when(roleService.findById(1)).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new ResponseObject(200, "Data Found", newRole)));
		assertEquals(newRole, roleController.findById(1).getBody().getData());
	}
    
    @Test
	void testGetAllRoles() {
		lenient().when(roleService.getAllRole()).thenReturn(ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseListRole(404, "Role is empty")));
		assertEquals("Role is empty", roleController.getAllRole().getBody().getMessage());
	}
    
    @Test
	void testEditRole() {
    	Role updateRole = new Role();
		lenient().when(roleService.editRole(1, updateRole)).thenReturn(ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject(HttpStatus.OK.value(), "Update Role Success")));
		assertEquals("Update Role Success", roleController.editRole(1, updateRole).getBody().getMessage());
	}
    
    @Test
	void testDeactiveRole() {
		lenient().when(roleService.deactiveRole(1)).thenReturn(ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject(HttpStatus.OK.value(), "Deactive Role, Success")));
		assertEquals("Deactive Role, Success", roleController.deactiveRole(1).getBody().getMessage());
	}
    
    @Test
	void testGetRoleExceptRoleUser() {
		lenient().when(roleService.getRoleExceptRoleUser()).thenReturn(new ResponseObject(HttpStatus.OK.value(), "Success", roles));
		assertEquals(roles, roleController.getRoleExceptRoleUser().getBody().getData());
	}
    
    

	@Test
	void testGetAllDesks() {
		lenient().when(deskService.getAll()).thenReturn(new ResponseObject(HttpStatus.OK.value(), "Success", desks));
		assertEquals(desks, deskController.getAllDesks().getBody().getData());
	}
	
	@Test
	void testGetActive() {
		lenient().when(deskService.getAllActive()).thenReturn(new ResponseObject(HttpStatus.OK.value(), "Success", desks));
		assertEquals(desks, deskController.getActive().getBody().getData());
	}
	
	@Test
	void testGetmaxNop() {
		lenient().when(deskService.getMaxNop()).thenReturn(new ResponseObject(HttpStatus.OK.value(), "Success", 4));
		assertEquals(4, deskController.getmaxNop().getBody().getData());
	}
	
	@Test
	void testGetActiveByNop() {
		lenient().when(deskService.getActiveByNop("2")).thenReturn(new ResponseObject(HttpStatus.OK.value(), "Success", desks));
		assertEquals(desks, deskController.getActiveByNop("2").getBody().getData());
	}
	
	@Test
	void testGet() {
		lenient().when(deskService.getDeskById("2")).thenReturn(new ResponseObject(HttpStatus.OK.value(), "Success", desks));
		assertEquals(desks, deskController.get("2").getBody().getData());
	}
	
	@Test
	void testGetAvailableForTransaction() {
		lenient().when(deskService.getAvailableForTr("2", "2021-11-25", "13:00:00")).thenReturn(new ResponseObject(HttpStatus.OK.value(), "Success", desks));
		assertEquals(desks, deskController.getAvailableForTransaction("2", "2021-11-25", "13:00:00").getBody().getData());
	}
	
	@Test
	void testAdd() {
		Desk newDesk = new Desk();
		lenient().when(deskService.add(newDesk)).thenReturn(new ResponseObject(HttpStatus.OK.value(), "Success", newDesk));
		assertEquals(newDesk, deskController.add(newDesk).getBody().getData());
	}
	
	@Test
	void testEdit() {
		Desk newDesk = new Desk();
		lenient().when(deskService.update("1", newDesk)).thenReturn(new ResponseObject(HttpStatus.OK.value(), "Success", newDesk));
		assertEquals(newDesk, deskController.edit("1", newDesk).getBody().getData());
	}
	
	@Test
	void testDelete() {
		Desk deletedDesk = new Desk();
		lenient().when(deskService.delete("1")).thenReturn(new ResponseObject(HttpStatus.OK.value(), "Success", deletedDesk));
		assertEquals(deletedDesk, deskController.delete("1").getBody().getData());
	}
	
	@Test
	void testFindByNumberOfPeople() {
		lenient().when(deskService.findByNumberOfPeople("2")).thenReturn(new ResponseObject(HttpStatus.OK.value(), "Success", desks));
		assertEquals(desks, deskController.findByNumberOfPeople("2").getBody().getData());
	}
	
	@Test
	void testUpdateStatusAvailable() {
		ResponseEntity<ResponseObject> res = ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject(HttpStatus.OK.value(), "Update Status Available, Success"));
		lenient().when(deskService.updateStatusAvailable(2)).thenReturn(res);
		assertEquals(res, deskController.updateStatusAvailable(2));
	}
	
	@Test
	void testGetAll() {
		lenient().when(deskRepo.findAll()).thenReturn(desks);
		assertEquals(desks, deskServiceInject.getAll().getData());
	}
	
	@Test
	void testGetAllActive() {
		Desk a = new Desk();
		a.setDeleted_at(new Timestamp(1L));
		Desk b = new Desk();
		desks.add(a);
		desks.add(b);
		lenient().when(deskRepo.findAll()).thenReturn(desks);
		List<Desk> filteredDesk = desks.stream().filter(desk -> desk.getDeleted_at() == null).collect(Collectors.toList());
		assertEquals(filteredDesk, deskServiceInject.getAllActive().getData());
	}
	
	@Test
	void getDeskByIdNull() {
		lenient().when(deskRepo.findById(1)).thenReturn(null);
		assertEquals(null, deskServiceInject.getDeskById("1").getData());
	}
	
	@Test
	void getDeskByIdNotNull() {
		Desk a = new Desk();
		a.setId(1);
		lenient().when(deskRepo.findByID(1)).thenReturn(a);
		assertEquals(a, deskServiceInject.getDeskById("1").getData());
	}

}
