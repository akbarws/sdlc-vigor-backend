package com.sdlc.vigor.service;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.sdlc.vigor.entity.Desk;
import com.sdlc.vigor.entity.Packet;
import com.sdlc.vigor.entity.Transaction;
import com.sdlc.vigor.entity.TransactionDetail;
import com.sdlc.vigor.entity.TransactionType;
import com.sdlc.vigor.entity.User;
import com.sdlc.vigor.repository.DeskRepository;
import com.sdlc.vigor.repository.PacketRepository;
import com.sdlc.vigor.repository.TransactionDetailRepository;
import com.sdlc.vigor.repository.TransactionRepository;
import com.sdlc.vigor.repository.TransactionTypeRepository;
import com.sdlc.vigor.repository.UserRepository;
import com.sdlc.vigor.response.ResponseObject;
import com.midtrans.Config;
import com.midtrans.ConfigFactory;
import com.midtrans.httpclient.error.MidtransError;
import com.midtrans.service.MidtransSnapApi;

@Service
public class TransactionService {

	@Autowired
	TransactionRepository repo;

	@Autowired
	DeskRepository deskRepo;

	@Autowired
	UserRepository userRepo;

	@Autowired
	TransactionTypeRepository trTypeRepo;

	@Autowired
	TransactionDetailRepository trDetailRepo;

	@Autowired
	PacketRepository packetRepository;

	@Autowired
	EmailSenderService mail;

	public ResponseObject getAll() {
		return new ResponseObject(HttpStatus.OK.value(), "Success", repo.findAll());
	}

	public ResponseObject getById(String id) {
		return new ResponseObject(HttpStatus.OK.value(), "Success", repo.findById(Integer.valueOf(id)));
	}

	// Skenario 1 Transaksi User Online
	public ResponseObject addTrUserOnline(int idU, int idTrType, int idDesk, Transaction tr) {
		User u = userRepo.findById(idU);

		// type online, offline -> diatur dr front end
		TransactionType trT = trTypeRepo.findById(idTrType);
		Desk d = deskRepo.findByID(idDesk);

		String tgl = tr.getTanggalBooking();
		String jam = tr.getJamBooking();

		List<TransactionDetail> details = tr.getTransactionDetails();

		int total = 0;
		for (TransactionDetail detail : details) {
			Packet packet = packetRepository.getById(detail.getPacket().getId());
			total = total + detail.getQty() * packet.getPrice();
			detail.setPacket(packet);
		}
		tr.setGrandTotal(total);

		// set status blm lunas dulu, nanti DIUPDATE saat payment midTrans
		tr.setStatus(false);

		Transaction trans = new Transaction(u, trT, d, tr.getGrandTotal(), tr.getStatus(), tr.getInvoiceUrl(), 0, 0, 0,
				tgl, jam);
		repo.save(trans);

		for (TransactionDetail detaill : details) {
			detaill.setTransaction(trans);
			trDetailRepo.save(detaill);
		}

		// generate e-ticket
		trans.setTransactionDetails(details);
		mail.createInvoice(trans);
		mail.sendEmailWithAttachment("simanihurukreena@gmail.com", "E-Ticket Customer" + u.getId(),
				trans.getId());

		checkoutMidTrans(trans.getId());

		return new ResponseObject(HttpStatus.OK.value(), "Success", trans);
	}

	// Skenario 3 (Transaksi User Offline - input dari waiter)
	public ResponseObject addTrUserOffline(int idU, int idTrType, int idDesk, Transaction tr) {
		User u = userRepo.findById(idU);

		// type online, offline -> diatur dr front end
		TransactionType trT = trTypeRepo.findById(idTrType);
		Desk d = deskRepo.findByID(idDesk);

		String tgl = tr.getTanggalBooking();
		String jam = tr.getJamBooking();

		List<TransactionDetail> details = tr.getTransactionDetails();

		int total = 0;
		for (TransactionDetail detail : details) {
			Packet packet = packetRepository.getById(detail.getPacket().getId());
			total = total + detail.getQty() * packet.getPrice();
			detail.setPacket(packet);
		}
		tr.setGrandTotal(total);

		// set status blm lunas dulu, nanti DIUPDATE saat payment midTrans
		tr.setStatus(false);

		Transaction trans = new Transaction(u, trT, d, tr.getGrandTotal(), tr.getStatus(), tr.getInvoiceUrl(), 0, 0, 0,
				tgl, jam);
		repo.save(trans);

		for (TransactionDetail detaill : details) {
			detaill.setTransaction(trans);
			trDetailRepo.save(detaill);
		}

		return new ResponseObject(HttpStatus.OK.value(), "Success", trans);
	}

	// ad tr delivery
	public ResponseObject addTrUserDelivery(int idU, int idTrType, Transaction tr) {
		User u = userRepo.findById(idU);
		// type delivery -> diatur dr front end
		TransactionType trT = trTypeRepo.findById(idTrType);

		String tgl = tr.getTanggalBooking();
		String jam = tr.getJamBooking();

		List<TransactionDetail> details = tr.getTransactionDetails();

		int total = 0;
		for (TransactionDetail detail : details) {
			Packet packet = packetRepository.getById(detail.getPacket().getId());
			total = total + detail.getQty() * packet.getPrice();
			detail.setPacket(packet);
		}
		tr.setGrandTotal(total);

		// set status blm lunas dulu, nanti DIUPDATE saat payment midTrans
		tr.setStatus(false);

		Transaction trans = new Transaction(u, trT, null, tr.getGrandTotal(), tr.getStatus(), tr.getInvoiceUrl(), 0, 0,
				0, tgl, jam);
		repo.save(trans);

		for (TransactionDetail detaill : details) {
			detaill.setTransaction(trans);
			trDetailRepo.save(detaill);
		}

		trans.setTransactionDetails(details);
		mail.createInvoice(trans);
		mail.sendEmailWithAttachment("simanihurukreena@gmail.com", "E-Ticket Customer" + u.getId(),
				trans.getId());

		return new ResponseObject(HttpStatus.OK.value(), "Success");
	}

	// Skenario 2 (Transaksi User Online nambah temen)
	public ResponseObject addTrUserOnlineNambahTemen(int idTrTemen, int idU, int idTrType, String codeDesk,
			Transaction tr) {
		User u = userRepo.findById(idU);
		if (u == null) {
			return new ResponseObject(HttpStatus.BAD_REQUEST.value(), "User not found");
		}

		// type offline by waiter -> diatur dr front end
		TransactionType trT = trTypeRepo.findById(idTrType);

		// SM KAYAK TMNNYA (di front end input kode mejanya lgsg)
		Desk d = deskRepo.findByCodee(codeDesk);

		Transaction trUser = repo.findById(idTrTemen); // ????

		String tgl = trUser.getTanggalBooking();
		String jam = trUser.getJamBooking();

		List<TransactionDetail> details = tr.getTransactionDetails();

		int total = 0;
		for (TransactionDetail detail : details) {
			Packet packet = packetRepository.getById(detail.getPacket().getId());
			total = total + detail.getQty() * packet.getPrice();
			detail.setPacket(packet);
		}
		tr.setGrandTotal(total);

		// set status blm lunas dulu, nanti DIUPDATE saat payment DI CASHIER
		tr.setStatus(false);

		Transaction trans = new Transaction(u, trT, d, tr.getGrandTotal(), tr.getStatus(), tr.getInvoiceUrl(), 0, 0, 0,
				tgl, jam);
		repo.save(trans);

		for (TransactionDetail detaill : details) {
			detaill.setTransaction(trans);
			trDetailRepo.save(detaill);
		}

		return new ResponseObject(HttpStatus.OK.value(), "Success");
	}

	// User checkout check penalty
	public ResponseObject updateTableByPenalty(int id, Transaction payload) {
		Transaction transactionFromDb = repo.findById(id);

		if (transactionFromDb == null) {
			return new ResponseObject(HttpStatus.OK.value(), "Transaction not found");
		}

		float overTime = payload.getOvertime(); // denda 160k per 30 menit
		float leftOver = payload.getLeftover(); // denda 160k per 100gram

		float hitungOverTime = 0;
		float hitungLeftOver = 0;
		float totalPenalty = 0;

		if (overTime < 30 && leftOver < 100) {
			return new ResponseObject(HttpStatus.OK.value(), "Congrats, you are not punished. Thankyou..");
		} else if (overTime >= 30 && leftOver >= 100) {
			hitungOverTime = (overTime / 30) * 160000;
			hitungLeftOver = (leftOver / 100) * 160000;
			totalPenalty = hitungOverTime + hitungLeftOver;
		} else if (overTime >= 30) {
			hitungOverTime = (overTime / 30) * 160000;
			totalPenalty = hitungOverTime;
		} else if (leftOver >= 100) {
			hitungLeftOver = (leftOver / 100) * 160000;
			totalPenalty = hitungLeftOver;
		}

		Transaction transaction = transactionFromDb;
		transaction.setOvertime(hitungOverTime);
		transaction.setLeftover(hitungLeftOver);
		transaction.setTotalPenalty(totalPenalty);

		float grandTotal = transaction.getGrandTotal();
		float totalP = transaction.getTotalPenalty();
		float finalPayment = grandTotal + totalP;

		transaction.setGrandTotal(finalPayment);

		repo.save(transaction);

		User u = userRepo.findById(transaction.getUser().getId());

		// generate invoice
//		transaction.setTransactionDetails(details);
		mail.createInvoice2(transaction);
		mail.sendEmailWithAttachment2("simanihurukreena@gmail.com", "Transaction Report Customer" + u.getId(),
				transaction.getId());

		return new ResponseObject(HttpStatus.OK.value(), "Success", transaction);
	}

	// by cashier or midtrans
	public Transaction updateStatusPaymentTrue(int id) {
		Transaction transactionFromDb = repo.findById(id);

		transactionFromDb.setStatus(true);
		repo.save(transactionFromDb);

		return transactionFromDb;
	}

	public ResponseObject updateTable(String id, String deskId) {
		Optional<Transaction> transactionFromDb = repo.findById(Integer.valueOf(id));

		if (transactionFromDb.isEmpty())
			return new ResponseObject(HttpStatus.OK.value(), "Transaction not found");

		Transaction transaction = transactionFromDb.get();

		Optional<Desk> deskFromDb = deskRepo.findById(Integer.valueOf(deskId));

		if (deskFromDb.isEmpty())
			return new ResponseObject(HttpStatus.OK.value(), "Desk not found");

		Desk desk = deskFromDb.get();

		transaction.setDesk(desk);
		repo.save(transaction);

		return new ResponseObject(HttpStatus.OK.value(), "Success", transaction);
	}

	public ResponseObject getTableByDateAndUserNameAndBelumLunas(String deskCode, String jam_booking) {
		long time = System.currentTimeMillis();
		Timestamp t = new Timestamp(time);
		String now = t.toString().split(" ")[0];
		Desk desk = deskRepo.findByCodee(deskCode);

		if (desk == null) {
			return new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Desk not found");
		}

		Transaction transaction = repo.findByTanggalJamDeskStatus(now, jam_booking, desk.getId());

		if (transaction == null) {
			return new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Transaction not found");
		}
		Transaction tr = updateStatusPaymentTrue(transaction.getId());

		return new ResponseObject(HttpStatus.OK.value(), "Success", tr);
	}

	public ResponseObject getByUserId(String userId) {
		Optional<List<Transaction>> transactions = repo.findByUserId(Integer.valueOf(userId));
		return new ResponseObject(HttpStatus.OK.value(), "Success", transactions);
	}

	public String midTrans(String order_id, float amount) {
		Map<String, Object> params = new HashMap<>();
		Map<String, String> transactionDetails = new HashMap<>();
		transactionDetails.put("order_id", order_id);

		int value = (int) amount;

		transactionDetails.put("gross_amount", String.valueOf(value));
		params.put("transaction_details", transactionDetails);
		try {
			MidtransSnapApi snapApi = new ConfigFactory(
					new Config("SB-Mid-server-d4jUiLxuoSa4Uap8oXV8lL2R", "SB-Mid-client-TcXnB89obum6wM0g", false))
							.getSnapApi();
			return snapApi.createTransactionToken(params);
		} catch (MidtransError e) {
			return e.getMessage();
		}
	}

	public ResponseObject checkoutMidTrans(int id) {
		Transaction transaction = repo.findById(id);
		List<TransactionDetail> detailTr = trDetailRepo.findByIdTr(transaction.getId());
		int total = 0;
		for (TransactionDetail dT : detailTr) {
			total = total + dT.getPacket().getPrice() * dT.getQty();
		}
		UUID orderId = UUID.randomUUID();
		transaction.setGrandTotal(total);
		transaction.setOrderId(orderId.toString());
		String paymentToken = midTrans(transaction.getOrderId(), transaction.getGrandTotal());

		transaction.setPaymentToken(paymentToken);
		repo.save(transaction);
		return new ResponseObject(HttpStatus.OK.value(), "Success");
	}
}
