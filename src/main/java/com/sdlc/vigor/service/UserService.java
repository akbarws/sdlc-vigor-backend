package com.sdlc.vigor.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.sdlc.vigor.config.JwtUtil;
import com.sdlc.vigor.config.WebSecurityConfig;
import com.sdlc.vigor.entity.Role;
import com.sdlc.vigor.entity.User;
import com.sdlc.vigor.entity.UserLogin;
import com.sdlc.vigor.repository.RoleRepository;
import com.sdlc.vigor.repository.UserRepository;
import com.sdlc.vigor.response.ResponseObject;
import com.sdlc.vigor.response.ResponseListUser;

@Service
public class UserService {
	private final AuthenticationManager authenticationManager;
	private final JwtUtil jwtTokenUtil;
	private final MyUserDetailService userDetailsService;

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private RoleRepository roleRepo;

	@Autowired
	private WebSecurityConfig passwordEncoder;

	public UserService(AuthenticationManager authenticationManager, JwtUtil jwtTokenUtil,
			MyUserDetailService userDetailsService) {
		this.authenticationManager = authenticationManager;
		this.jwtTokenUtil = jwtTokenUtil;
		this.userDetailsService = userDetailsService;
	}

	public ResponseEntity<ResponseObject> createAuthenticationToken(@RequestBody User user) throws Exception {
		User cek = userRepo.findByEmail(user.getEmail());
		try {

			if (cek == null) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseObject(404, "Data Not Found"));
			}

			Authentication authentication = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword()));
			UserDetails userDetails = userDetailsService.loadUserByUsername(user.getEmail());
			final String jwt = jwtTokenUtil.generateToken(userDetails);
			
			UserLogin userLogin = new UserLogin(cek.getId(), cek.getName(), cek.getRole().getName(), jwt);

			return ResponseEntity.status(HttpStatus.OK)
					.body(new ResponseObject(HttpStatus.OK.value(), "Login Success", userLogin));

		} catch (Exception e) {
			throw new Exception("Incorrect username or password", e);
		}
	}

	public ResponseEntity<ResponseObject> save(User user) {
		boolean regexEmail = Pattern.compile("[A-Za-z0-9]+[@]+[a-z]+[.]+[c][o][m]").matcher(user.getEmail()).find();
		boolean regexPassword = Pattern
				.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$")
				.matcher(user.getPassword()).find();
		boolean regexPhone = Pattern.compile("^(0|8[1-9][0-9]*)$").matcher(user.getPhone()).find();

		if (regexEmail && regexPassword && regexPhone) {
			
			if(userRepo.findByUsername(user.getName()) != null) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseObject(400, "Register Failed, name already exist"));
			}
			
			if(userRepo.findByEmail(user.getEmail()) != null) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseObject(400, "Register Failed, email already exist"));
			}
			
			user.setPassword(passwordEncoder.passwordEncoder().encode(user.getPassword()));
			Role role = roleRepo.findByName("ROLE_USER");
			user.setRole(role);
			userRepo.save(user);

			return ResponseEntity.status(HttpStatus.OK)
					.body(new ResponseObject(HttpStatus.OK.value(), "Register Success", user));
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseObject(400, "Register Failed"));
	}

	// add employee by admin
	public ResponseEntity<ResponseObject> addEmployee(int idRole, User user) {
		boolean regexEmail = Pattern.compile("[A-Za-z0-9]+[@]+[a-z]+[.]+[c][o][m]").matcher(user.getEmail()).find();
		boolean regexPassword = Pattern
				.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$")
				.matcher(user.getPassword()).find();
		boolean regexPhone = Pattern.compile("^(0|8[1-9][0-9]*)$").matcher(user.getPhone()).find();


		if (regexEmail && regexPassword && regexPhone) {
			if(userRepo.findByUsername(user.getName()) != null) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseObject(400, "Register Failed, name already exist"));
			}
			
			if(userRepo.findByEmail(user.getEmail()) != null) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseObject(400, "Register Failed, email already exist"));
			}
			user.setPassword(passwordEncoder.passwordEncoder().encode(user.getPassword()));
			
			Role findIdRole = roleRepo.findById(idRole);
			
			if(findIdRole == null) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseObject(404, "Role not found"));
			}
			
			user.setRole(findIdRole);
			userRepo.save(user);

			return ResponseEntity.status(HttpStatus.OK)
					.body(new ResponseObject(HttpStatus.OK.value(), "Add Employee Success", user));
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseObject(400, "Add Employee Failed"));
	}

	public ResponseEntity<ResponseListUser> getAllUser() {
		List<User> result = userRepo.findAll();
		if (result.size() == 0) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseListUser(404, "Data Not Found"));
		}
		return ResponseEntity.status(HttpStatus.OK).body(new ResponseListUser(200, "Data Found", result));
	}

	public ResponseEntity<ResponseObject> updateUser(int id, User payload) {
		User userId = userRepo.findById(id);

		if (userId == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseObject(404, "User Not Found"));
		}

		if (payload.getName() != null) {
			userId.setName(payload.getName());
		}

		if (payload.getEmail() != null) {
			userId.setEmail(payload.getEmail());
		}

		if (payload.getPhone() != null) {
			userId.setPhone(payload.getPhone());
		}

		if (payload.getAddress() != null) {
			userId.setAddress(payload.getAddress());
		}

		if (payload.getPassword() != null) {
			userId.setPassword(passwordEncoder.passwordEncoder().encode(payload.getPassword()));
		}

		userRepo.updateUser(id, userId.getName(), userId.getEmail(), userId.getPhone(), userId.getAddress(),
				userId.getPassword());
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject(HttpStatus.OK.value(), "Update User Success"));
	}

	public ResponseEntity<ResponseObject> deactiveUser(int id) {
		User userId = userRepo.findById(id);

		if (userId == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseObject(404, "User Not Found"));
		}

		long time = System.currentTimeMillis();

		userId.setDeleted_at(new Timestamp(time));

		userRepo.deactiveUser(id, userId.getDeleted_at());
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject(HttpStatus.OK.value(), "Deactive User, Success"));
	}

	public ResponseEntity<ResponseListUser> getUserExcept() {
		List<User> result = userRepo.findAllByRole();
		if (result.size() == 0) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseListUser(404, "Data Not Found"));
		}
		return ResponseEntity.status(HttpStatus.OK).body(new ResponseListUser(200, "Data Found", result));
	}

	public ResponseEntity<ResponseObject> findById(int id) {
		User userId = userRepo.findById(id);

		if (userId == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseObject(404, "Data Not Found"));
		}

		return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject(200, "Data Found", userId));
	}

	public User findByUsername(String username) {
		return userRepo.findByUsername(username);
	}

	public User findByEmail(String email) {
		return userRepo.findByEmail(email);
	}
}