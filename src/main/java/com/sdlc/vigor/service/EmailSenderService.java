package com.sdlc.vigor.service;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Locale;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailParseException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Table;
import com.lowagie.text.alignment.HorizontalAlignment;
import com.lowagie.text.alignment.VerticalAlignment;
import com.lowagie.text.pdf.PdfWriter;
import com.sdlc.vigor.entity.Transaction;
import com.sdlc.vigor.entity.TransactionDetail;

@Service
public class EmailSenderService {

	@Autowired
	private JavaMailSender mailSender;

	public void createInvoice(Transaction tr) {
		String path = "C:/Users/NEXSOFT/InvoiceRestoApp/E-Ticket_Vigor" + tr.getId() + ".pdf";

		Document document = null;
		try {
			document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(path));
//			Font font = new Font(Font.HELVETICA, 12, Font.BOLD);

			document.open();
			
			Paragraph judul = new Paragraph("E - Ticket");
			judul.setAlignment("center");
			document.add(judul);
			
			Paragraph date = new Paragraph("\nOrdered on : " + LocalDate.now());
			document.add(date);

			Paragraph time = new Paragraph("Time : " + LocalTime.now());
			document.add(time);

			Paragraph namaUser = new Paragraph("\nName : " +tr.getUser().getName());
			document.add(namaUser);
			
			Paragraph codeMeja = new Paragraph("Desk Code : " +tr.getDesk().getCode());
			document.add(codeMeja);
			
			Paragraph ruler = new Paragraph("\n======================");
			document.add(ruler);
			
			Paragraph note = new Paragraph("\nDon't forget to come on : ");
			document.add(note);
			
			Paragraph tanggalBook = new Paragraph("Date : " +tr.getTanggalBooking());
			document.add(tanggalBook);
			
			Paragraph jamBook = new Paragraph("Time : " +tr.getJamBooking());
			document.add(jamBook);
				
			
			Table table = new Table(12);
			table.setPadding(5);
			table.setSpacing(1);
			table.setWidth(100);
	        table.setBorderColor(new Color(10, 110, 120));

			// Setting table headers
			Cell cell = new Cell("Packet");
			cell.setHeader(true);
			cell.setVerticalAlignment(VerticalAlignment.CENTER);
			cell.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cell.setColspan(12);
			cell.setBackgroundColor(Color.GRAY);
			table.addCell(cell);
			
//		    Data information tr to table cells
			List<TransactionDetail> details = tr.getTransactionDetails();
			for (TransactionDetail t : details) {
				cell = new Cell(t.getPacket().getName());
				cell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				cell.setColspan(12);
				table.addCell(cell);	
			}
			
			document.add(table);
			document.close();
		} catch (DocumentException | FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void createInvoice2(Transaction tr) {
//		Locale locale = new Locale("idr", "IDR");      
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
				
		String path = "C:/Users/NEXSOFT/InvoiceRestoApp/Report_Vigor" + tr.getId() + ".pdf";

		Document document = null;
		try {
			document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(path));
//			Font font = new Font(Font.HELVETICA, 12, Font.BOLD);

			document.open();
			
			Paragraph judul = new Paragraph("REPORT TRANSACTION");
			judul.setAlignment("center");
			document.add(judul);
			
			Paragraph date = new Paragraph("Ordered on : " + LocalDate.now());
			document.add(date);

			Paragraph time = new Paragraph("Time : " + LocalTime.now());
			document.add(time);

			Table table = new Table(12);
			table.setPadding(5);
			table.setSpacing(1);
			table.setWidth(100);
	        table.setBorderColor(new Color(10, 110, 120));

			// Setting table headers
			Cell cell = new Cell("TRANSACTION");
			cell.setHeader(true);
			cell.setVerticalAlignment(VerticalAlignment.CENTER);
			cell.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cell.setColspan(12);
			cell.setBackgroundColor(Color.GRAY);
			table.addCell(cell);

			cell = new Cell("Order");
			cell.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cell.setColspan(4);
			table.addCell(cell);
			cell = new Cell("Quantity");
			cell.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cell.setColspan(4);
			table.addCell(cell);
			cell = new Cell("Price");
			cell.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cell.setColspan(4);
			table.addCell(cell);
			table.endHeaders();

//		    Data information tr to table cells
			List<TransactionDetail> details = tr.getTransactionDetails();
			for (TransactionDetail t : details) {
				cell = new Cell(t.getPacket().getName());
				cell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				cell.setColspan(4);
				table.addCell(cell);
				cell = new Cell(String.valueOf(t.getQty()));
				cell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				cell.setColspan(4);
				table.addCell(cell);
				String price = currencyFormatter.format(t.getPacket().getPrice());
				cell = new Cell(price);
				cell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				cell.setColspan(4);
				table.addCell(cell);
			}
			document.add(table);
			
			Paragraph penalty = new Paragraph("\nPenalty : ");
			penalty.setAlignment("right");
			document.add(penalty);
			
			Paragraph leftOver = new Paragraph("Left Over : " + tr.getLeftover() + " gram");
			leftOver.setAlignment("right");
			document.add(leftOver);
			
			Paragraph overTime = new Paragraph("Over time : " + tr.getOvertime() + " minutes");
			overTime.setAlignment("right");
			document.add(overTime);
			
			Paragraph totalPenal = new Paragraph("Total penalty : " + tr.getTotalPenalty() + " minutes");
			totalPenal.setAlignment("right");
			document.add(totalPenal);
			
			Paragraph ruler = new Paragraph("\n========================");
			ruler.setAlignment("right");
			document.add(ruler);
			
			Paragraph gT = new Paragraph("\nGrand Total : " + currencyFormatter.format(tr.getGrandTotal()));
			gT.setAlignment("right");
			document.add(gT);
			
			Paragraph ruler2 = new Paragraph("\n========================");
			ruler2.setAlignment("right");
			document.add(ruler2);
			
			Paragraph note = new Paragraph("\n*Additional fee :\nIDR 160,000 every 30 minutes and \nIDR 160,000 every 100 grams.");
			note.setAlignment("right");
			document.add(note);
			
			
			document.close();
		} catch (DocumentException | FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void sendEmailWithAttachment(String toEmail, String subject, int id) {
		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(message, true);
			helper.setFrom("simanihurukreena@gmail.com");
			helper.setTo(toEmail);
			helper.setText("Vigor Resto");
			helper.setSubject(subject);
			FileSystemResource file = new FileSystemResource(
					"C:/Users/NEXSOFT/InvoiceRestoApp/E-Ticket_Vigor" + id + ".pdf");
			helper.addAttachment(file.getFilename(), file);
		} catch (MessagingException e) {
			throw new MailParseException(e);
		}
		mailSender.send(message);
	}

	public void sendEmailWithAttachment2(String toEmail, String subject, int id) {
		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(message, true);
			helper.setFrom("simanihurukreena@gmail.com");
			helper.setTo(toEmail);
			helper.setText("Vigor Resto");
			helper.setSubject(subject);
			FileSystemResource file = new FileSystemResource(
					"C:/Users/NEXSOFT/InvoiceRestoApp/Report_Vigor" + id + ".pdf");
			helper.addAttachment(file.getFilename(), file);
		} catch (MessagingException e) {
			throw new MailParseException(e);
		}
		mailSender.send(message);
	}
}