package com.sdlc.vigor.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.sdlc.vigor.entity.Packet;
import com.sdlc.vigor.repository.PacketRepository;
import com.sdlc.vigor.response.ResponseObject;

@Service
public class PacketService {

	@Autowired
	PacketRepository repo;
	
	@Autowired
	private Environment env;
	
	public ResponseObject getAll() {
		return new ResponseObject(HttpStatus.OK.value(), "Success", repo.findAll());
	}
	
	public ResponseObject getAllActive() {
		List<Packet> packets = repo.findAll();
		return new ResponseObject(HttpStatus.OK.value(), "Success", 
				packets.stream().filter(packet -> packet.getDeleted_at()==null).collect(Collectors.toList()));
	}
	
	public ResponseObject getById(String id) {
		Optional<Packet> packetFromDb = repo.findById(Integer.valueOf(id));
		if(packetFromDb.isEmpty())
			return new ResponseObject(HttpStatus.OK.value(), "Packet not found");
		Packet packet = packetFromDb.get();
		return new ResponseObject(HttpStatus.OK.value(), "Success", packet);
	}

	public ResponseObject add(Packet newPacket, MultipartFile image) {
		//lokasi image
		String imageFolderPath = env.getProperty("projectpath");
		//unique packet name file + extension
		String uniquePacketName = "packet-" + LocalDate.now() + "-" + String.format("%03d", repo.count());
		Optional<String> hasExtension = getExtension(image.getOriginalFilename());
		if(hasExtension.isEmpty())
			return new ResponseObject(HttpStatus.OK.value(), "Undefined file type");
		String Extension = hasExtension.get();
		String filename = (uniquePacketName + "." + Extension).trim();
		Path path = Paths.get(imageFolderPath, filename);
		try {
			// upload file
			byte[] imageData = image.getBytes();
			Files.write(path, imageData);
			Files.move(path, path.resolveSibling(filename));
			String folderPathOnServer = "http://localhost:"+env.getProperty("server.port")+"/images/";
			String folderPathforFrontend = folderPathOnServer + filename;
			newPacket.setImg_url(folderPathforFrontend);
			Packet addedPacket = repo.save(newPacket);
			return new ResponseObject(HttpStatus.OK.value(), "Success", addedPacket);
		} catch (IOException e) {
			return new ResponseObject(HttpStatus.OK.value(), "Error while add");
		}
	}
	
	private Optional<String> getExtension(String filename) {
		return Optional.ofNullable(filename).filter(f -> f.contains(".")).map(name->name.substring(name.lastIndexOf(".") + 1));
	}
	
	public ResponseObject updateById(String id, Packet newPacket, MultipartFile image) {
		Optional<Packet> packetFromDb = repo.findById(Integer.valueOf(id));
		if(packetFromDb.isEmpty())
			return new ResponseObject(HttpStatus.OK.value(), "Cannot find packet");
		Packet oldPacket = packetFromDb.get();
		String folderPathforFrontend = null;
		if(!image.isEmpty()) {
			String oldFilename = getFileName(oldPacket.getImg_url());
			Path oldImageFilePath = Paths.get(env.getProperty("projectpath")+oldFilename);
			try {
				if(!Files.deleteIfExists(oldImageFilePath)) {
				}
			} catch (IOException e) {
				return new ResponseObject(HttpStatus.OK.value(), "Error while delete old image file");
			}		
			String imageFolderPath = env.getProperty("projectpath");
			//unique packet name file + extension
			String uniquePacketName = "packet-" + LocalDate.now() + "-" + String.format("%03d", repo.count());
			Optional<String> hasExtension = getExtension(image.getOriginalFilename());
			if(hasExtension.isEmpty())
				return new ResponseObject(HttpStatus.OK.value(), "Undefined file type");
			String Extension = hasExtension.get();
			String filename = (uniquePacketName + "." + Extension).trim();
			Path path = Paths.get(imageFolderPath, filename);
			try {
				// upload file
				byte[] imageData = image.getBytes();
				Files.write(path, imageData);
				Files.move(path, path.resolveSibling(filename));
				String folderPathOnServer = "http://localhost:"+env.getProperty("server.port")+"/images/";
				folderPathforFrontend = folderPathOnServer + filename;
			} catch (IOException e) {
				return new ResponseObject(HttpStatus.OK.value(), "Error while add");
			}
		}
		oldPacket.setName(newPacket.getName());
		oldPacket.setPrice(newPacket.getPrice());
		oldPacket.setDescription(newPacket.getDescription());
		if(folderPathforFrontend!=null)
			oldPacket.setImg_url(folderPathforFrontend);
		Packet updatedPacket = repo.save(oldPacket);
		return new ResponseObject(HttpStatus.OK.value(), "Success", updatedPacket);
	}
	
	private String getFileName(String uri) {
		return uri.substring(uri.lastIndexOf('/') + 1);
	}
	
	public ResponseObject deleteById(String id) {
		Optional<Packet> packetFromDb = repo.findById(Integer.valueOf(id));
		if(packetFromDb.isEmpty())
			return new ResponseObject(HttpStatus.OK.value(), "Packet not found");
		Packet packet = packetFromDb.get();
		packet.setDeleted_at(new Timestamp(System.currentTimeMillis()));
		Packet deletedPacket = repo.save(packet);
		return new ResponseObject(HttpStatus.OK.value(), "Success", deletedPacket);
	}
	
	public ResponseObject getTreePacket() {
		List<Packet> result = repo.getTreePacket();
		return new ResponseObject(HttpStatus.OK.value(), "Success", result);
	}
}
