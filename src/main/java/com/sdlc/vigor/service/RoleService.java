package com.sdlc.vigor.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.sdlc.vigor.entity.Role;
import com.sdlc.vigor.repository.RoleRepository;
import com.sdlc.vigor.response.ResponseListRole;
import com.sdlc.vigor.response.ResponseObject;

@Service
public class RoleService {
	
	@Autowired
	private RoleRepository roleRepo;

	
	public ResponseEntity<ResponseObject> addRole(Role role) {
		if(role.getName() != null) {
			role.setName(role.getName());
		}
		
		roleRepo.save(role);

		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject(HttpStatus.OK.value(), "Add Role Success", role));
	}
	
	public ResponseEntity<ResponseListRole> getAllRole() {
		List<Role> result = roleRepo.findAll();
		
		if (result.size() == 0) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseListRole(404, "Role is empty"));
		}
		
		System.out.println(result);
		return ResponseEntity.status(HttpStatus.OK).body(new ResponseListRole(200, "Data Found", result));
	}
	
	public ResponseEntity<ResponseObject> findById(int id) {
		Role roleId = roleRepo.findById(id);
		
		if(roleId == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseObject(404, "Role Not Found"));
		}
		
		return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject(200, "Data Found", roleId));
	}

	public ResponseEntity<ResponseObject> editRole(int id, Role payload) {
		Role roleId = roleRepo.findById(id);
		
		if(roleId == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseObject(404, "Role Not Found"));
		}
		
		if(payload.getName() == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Name is empty"));
		}
		roleId.setName(payload.getName());
		
		roleRepo.editRole(id, roleId.getName());
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject(HttpStatus.OK.value(), "Update Role Success"));
	}
	
	public ResponseEntity<ResponseObject> deactiveRole(int id){
		Role roleId = roleRepo.findById(id);
		
		if(roleId == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseObject(404, "Role Not Found"));
		}
		
		long time = System.currentTimeMillis();
		
		roleId.setDeleted_at(new Timestamp(time));
		
		
		roleRepo.deactiveRole(id, roleId.getDeleted_at());
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject(HttpStatus.OK.value(), "Deactive Role, Success"));
	}
	
	public ResponseObject getRoleExceptRoleUser() {
		List<Role> role = new ArrayList<Role>();
		role = roleRepo.getRoleExceptRoleUser();
		return new ResponseObject(HttpStatus.OK.value(), "Success", role);
	}

}
