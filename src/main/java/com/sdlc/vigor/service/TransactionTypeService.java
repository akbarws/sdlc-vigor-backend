package com.sdlc.vigor.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.sdlc.vigor.entity.TransactionType;
import com.sdlc.vigor.repository.TransactionTypeRepository;
import com.sdlc.vigor.response.ResponseObject;

@Service
public class TransactionTypeService {

	@Autowired
	TransactionTypeRepository repo;
	
	public ResponseObject getAll() {
		return new ResponseObject(HttpStatus.OK.value(), "Success", repo.findAll());
	}
	
	public ResponseObject getAllActive() {
		List<TransactionType> transactionTypes = repo.findAll();
		return new ResponseObject(HttpStatus.OK.value(), "Success", 
				transactionTypes.stream().filter(ttype -> ttype.getDeleted_at()!=null).collect(Collectors.toList()));
	}
	
	public ResponseObject getById(String id) {
		return new ResponseObject(HttpStatus.OK.value(), "Success", repo.findById(Integer.valueOf(id)));
	}
	
	public ResponseObject add(TransactionType transactionType) {
		return new ResponseObject(HttpStatus.OK.value(), "Success", repo.save(transactionType));
	}
	
	public ResponseObject edit(String id, TransactionType transactionType) {
		Optional<TransactionType> transactionTypeFromDB = repo.findById(Integer.valueOf(id));
		if(transactionTypeFromDB.isEmpty())
			return new ResponseObject(HttpStatus.OK.value(), "Transaction Type not found");
		TransactionType oldTransactionType = transactionTypeFromDB.get();
		oldTransactionType.setName(transactionType.getName());
		return new ResponseObject(HttpStatus.OK.value(), "Success", repo.save(oldTransactionType));
	}
	
	public ResponseObject delete(String id) {
		Optional<TransactionType> transactionTypeFromDB = repo.findById(Integer.valueOf(id));
		if(transactionTypeFromDB.isEmpty())
			return new ResponseObject(HttpStatus.OK.value(), "Transaction Type not found");
		TransactionType oldTransactionType = transactionTypeFromDB.get();
		oldTransactionType.setDeleted_at(new Timestamp(System.currentTimeMillis()));
		return new ResponseObject(HttpStatus.OK.value(), "Success", repo.save(oldTransactionType));
	}
}
