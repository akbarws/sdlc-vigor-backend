package com.sdlc.vigor.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sdlc.vigor.entity.MyUserDetails;
import com.sdlc.vigor.repository.UserRepository;
import com.sdlc.vigor.entity.User;

@Service
public class MyUserDetailService implements UserDetailsService {

	@Autowired
	private UserRepository userRepo;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Optional<User> user = Optional.ofNullable(userRepo.findByEmail(email));
		return user.map(MyUserDetails::new).get();
	}
}