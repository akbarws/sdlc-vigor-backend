package com.sdlc.vigor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.sdlc.vigor.entity.Shipment;
import com.sdlc.vigor.entity.ShipmentStatus;
import com.sdlc.vigor.entity.Transaction;
import com.sdlc.vigor.entity.User;
import com.sdlc.vigor.repository.ShipmentRepository;
import com.sdlc.vigor.repository.ShipmentStatusRepository;
import com.sdlc.vigor.repository.TransactionRepository;
import com.sdlc.vigor.repository.UserRepository;
import com.sdlc.vigor.response.ResponseObject;

@Service
public class ShipmentService {

	@Autowired
	ShipmentRepository repo;
	
	@Autowired
	ShipmentStatusRepository repoShipmentStatus;
	
	@Autowired
	TransactionRepository repoTr;
	
	@Autowired
	UserRepository repoUser;

	public ResponseObject getByTrId(String id) {
		Shipment searchTr = repo.findByTrId(Integer.valueOf(id));
		if(searchTr == null) {
			return new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Transaction not found");
		}
		return new ResponseObject(HttpStatus.OK.value(), "Success", repo.findByTrId(Integer.valueOf(id)));
	}
	
	public ResponseObject add(int trID, int shipmentStatusID, int userID) {
		Transaction tr = repoTr.findById(trID);
		if (tr == null) {
			return new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Transaction not found");
		}
		
		ShipmentStatus ss = repoShipmentStatus.findById(shipmentStatusID);

		if (ss == null) {
			return new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Shipment Status not found");
		}
		
		User u = repoUser.findById(userID);
		if (u == null) {
			return new ResponseObject(HttpStatus.BAD_REQUEST.value(), "User not found");
		}
		
		Shipment result = new Shipment(tr, ss, u);
		return new ResponseObject(HttpStatus.OK.value(), "Success", repo.save(result));
	}
}
