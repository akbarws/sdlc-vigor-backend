package com.sdlc.vigor.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.sdlc.vigor.entity.Desk;
import com.sdlc.vigor.repository.DeskRepository;
import com.sdlc.vigor.response.ResponseObject;
import org.springframework.core.env.Environment;

@Service
public class DeskService {

	@Autowired
	DeskRepository repo;

	public ResponseObject getAll() {
		List<Desk> desks = new ArrayList<Desk>();
		desks = repo.findAll();
		return new ResponseObject(HttpStatus.OK.value(), "Success", desks);
	}

	public ResponseObject getAllActive() {
		List<Desk> allDesk = repo.findAll();
		return new ResponseObject(HttpStatus.OK.value(), "Success",
				allDesk.stream().filter(desk -> desk.getDeleted_at() == null).collect(Collectors.toList()));
	}

	public ResponseObject getDeskById(String id) {
		Desk deskFromDb = repo.findByID(Integer.valueOf(id));
		if (deskFromDb==null) {
			return new ResponseObject(HttpStatus.OK.value(), "Desk empty");
		} else {
			return new ResponseObject(HttpStatus.OK.value(), "Success", deskFromDb);
		}
	}

	public ResponseObject add(Desk desk) {
		Optional<Desk> deskFromDb = repo.findByCode(desk.getCode());
		if (!deskFromDb.isEmpty())
			return new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Table code already had", deskFromDb);
		Desk newDesk = repo.save(desk);
		return new ResponseObject(HttpStatus.OK.value(), "Success", newDesk);
	}

	public ResponseObject update(String id, Desk desk) {
		// check ID
		Optional<Desk> deskFromDbById = repo.findById(Integer.valueOf(id));
		if (deskFromDbById.isEmpty())
			return new ResponseObject(HttpStatus.OK.value(), "Desk not found");
		// check code
		Desk oldDesk = deskFromDbById.get();
		Optional<Desk> anotherDeskWithSameCode = repo.findByCode(desk.getCode());
		if (!anotherDeskWithSameCode.isEmpty() && anotherDeskWithSameCode.get().getCode() != oldDesk.getCode())
			return new ResponseObject(HttpStatus.OK.value(), "Table code already had", anotherDeskWithSameCode.get());
		// check number of people
		if (desk.getNumberOfPeople() < 1)
			return new ResponseObject(HttpStatus.OK.value(), "Invalid Number of People < 1", desk);
		oldDesk.setCode(desk.getCode());
		oldDesk.setNumberOfPeople(desk.getNumberOfPeople());
		oldDesk.setStatus(desk.isStatus());
		oldDesk.setStatus_name(desk.getStatus_name());
		return new ResponseObject(HttpStatus.OK.value(), "Success", repo.save(oldDesk));
	}

	public ResponseObject delete(String id) {
		Optional<Desk> deskFromDb = repo.findById(Integer.valueOf(id));
		if (deskFromDb.isEmpty())
			return new ResponseObject(HttpStatus.OK.value(), "Desk not found");
		Desk desk = deskFromDb.get();
		desk.setDeleted_at(new Timestamp(System.currentTimeMillis()));
		Desk updatedDesk = repo.save(desk);
		return new ResponseObject(HttpStatus.OK.value(), "Success", updatedDesk);
	}
	
	public ResponseObject getMaxNop() {
		int maxNop = repo.getMaxNop();
		return new ResponseObject(HttpStatus.OK.value(), "Success", maxNop); 
	}

	public ResponseObject getActiveByNop(String numberOfPeople) {
		Optional<List<Desk>> desksFromDb = repo.findByNop(Integer.valueOf(numberOfPeople)); 
		return new ResponseObject(HttpStatus.OK.value(), "Success", desksFromDb);
	}
	
	public ResponseObject getAvailableForTr(String nop, String tb, String jb) {
		Optional<List<Desk>> desksFromDb = repo.findForTransaction(Integer.valueOf(nop), tb, jb);
		if(desksFromDb.isPresent()) {
			return new ResponseObject(HttpStatus.OK.value(), "Success", desksFromDb.get());
		} else
			return new ResponseObject(HttpStatus.OK.value(), "Success", new ArrayList<Desk>());
	}


	public ResponseObject findByNumberOfPeople(String people) {
		List<Desk> filterDesk = repo.findByNumberOfPeople(Integer.parseInt(people));
		if (filterDesk.isEmpty()) {
			return new ResponseObject(HttpStatus.OK.value(), "Desk empty");
		} else {
			return new ResponseObject(HttpStatus.OK.value(), "Success", filterDesk);
		}
	}
	
	public ResponseEntity<ResponseObject> updateStatusAvailable(int id) {
		Desk idDesk = repo.findByID(id);
		
		if(idDesk == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseObject(404, "Desk Not Found"));
		}
		
		repo.updateStatusAvailable(id);
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject(HttpStatus.OK.value(), "Update Status Available, Success"));
	}
	
	public ResponseEntity<ResponseObject> updateStatusToBooking(int id) {
		Desk idDesk = repo.findByID(id);
		
		if(idDesk == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseObject(404, "Desk Not Found"));
		}
		
		repo.updateStatusToBooking(id);
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject(HttpStatus.OK.value(), "Update Status Available, Success"));
	}
	
	public ResponseEntity<ResponseObject> updateStatusToMaintained(int id) {
		Desk idDesk = repo.findByID(id);
		
		if(idDesk == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseObject(404, "Desk Not Found"));
		}
		
		repo.updateStatusToMaintained(id);
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject(HttpStatus.OK.value(), "Update Status Available, Success"));
	}
}
