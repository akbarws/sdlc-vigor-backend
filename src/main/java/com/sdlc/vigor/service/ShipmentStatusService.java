package com.sdlc.vigor.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.sdlc.vigor.entity.ShipmentStatus;
import com.sdlc.vigor.repository.ShipmentStatusRepository;
import com.sdlc.vigor.response.ResponseObject;

@Service
public class ShipmentStatusService {

	@Autowired
	ShipmentStatusRepository repo;

	public ResponseObject getAll() {
		return new ResponseObject(HttpStatus.OK.value(), "Success", repo.findAll());
	}

	public ResponseObject getAllActive() {
		List<ShipmentStatus> ShipmentStatusFromDB = repo.findAll();
		return new ResponseObject(HttpStatus.OK.value(), "Success", ShipmentStatusFromDB.stream()
				.filter(ShipmentStatus -> ShipmentStatus.getDeleted_at() == null).collect(Collectors.toList()));
	}

	public ResponseObject getById(String id) {
		return new ResponseObject(HttpStatus.OK.value(), "Success", repo.findById(Integer.valueOf(id)));
	}

	public ResponseObject add(ShipmentStatus shipmentStatus) {
		return new ResponseObject(HttpStatus.OK.value(), "Success", repo.save(shipmentStatus));
	}

	public ResponseObject edit(String id, ShipmentStatus shipmentStatus) {
		Optional<ShipmentStatus> shipmentStatusFromDb = repo.findById(Integer.valueOf(id));
		if (shipmentStatusFromDb.isEmpty())
			return new ResponseObject(HttpStatus.OK.value(), "Shipment Status not found");
		ShipmentStatus oldShipmentStatus = shipmentStatusFromDb.get();
		oldShipmentStatus.setName(shipmentStatus.getName());
		ShipmentStatus updatedShipmentStatus = repo.save(oldShipmentStatus);
		return new ResponseObject(HttpStatus.OK.value(), "Success", updatedShipmentStatus);
	}

	public ResponseObject delete(String id) {
		Optional<ShipmentStatus> shipmentStatusFromDb = repo.findById(Integer.valueOf(id));
		if (shipmentStatusFromDb.isEmpty())
			return new ResponseObject(HttpStatus.OK.value(), "Shipment Status not found");
		ShipmentStatus oldShipmentStatus = shipmentStatusFromDb.get();
		oldShipmentStatus.setDeleted_at(new Timestamp(System.currentTimeMillis()));
		ShipmentStatus updatedShipmentStatus = repo.save(oldShipmentStatus);
		return new ResponseObject(HttpStatus.OK.value(), "Success", updatedShipmentStatus);
	}

}
