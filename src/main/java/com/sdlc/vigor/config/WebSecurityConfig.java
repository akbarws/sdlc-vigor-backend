package com.sdlc.vigor.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.sdlc.vigor.service.MyUserDetailService;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
    private MyUserDetailService myUserDetailService;

    @Autowired
    private JwtRequestFilter jwtRequestFilter;


    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(myUserDetailService);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.csrf().disable()
    			.authorizeRequests()
    			.antMatchers("/api/transaction/updateStatus/**").permitAll()
    			.antMatchers(
    					"/api/public/**",
    					"/images/**",
    					"/api/desk/**"
    					).permitAll()
    			.antMatchers(
    					"/api/users",
    					"/api/user/**"
    					).hasRole("USER")
    			.antMatchers(
    					"/api/admin/**",
    					"/api/roles",
    					"/api/role/**"
    					).hasRole("ADMIN")
    			.antMatchers(
    					"/api/desks",
    					"/api/packets",
    					"/api/transtypes/**",
    					"/api/transtype/**"
    					).hasAnyRole("USER", "ADMIN", "WAITER")
    			.antMatchers(
    					"/api/transactions",
    					"/api/transaction/**",
    					"/api/transtypes",
    					"/api/transtype/**"
    					).hasAnyRole("USER", "WAITER", "CASHIER", "ADMIN")
    			.anyRequest()
				.authenticated().and().exceptionHandling()
				.and().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
	}
}