package com.sdlc.vigor.response;

import java.util.List;

import com.sdlc.vigor.entity.User;

public class ResponseListUser implements Response {
	private int status;
	private String message;
	private List<User> data;
	
	public ResponseListUser() {
	}
	
	public ResponseListUser(int status, String message, List<User> data) {
		this.status = status;
		this.message = message;
		this.data = data;
	}
	
	public ResponseListUser(int status, String message) {
		this.status = status;
		this.message = message;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<User> getData() {
		return data;
	}

	public void setData(List<User> data) {
		this.data = data;
	}
}
