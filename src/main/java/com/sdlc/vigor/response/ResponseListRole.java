package com.sdlc.vigor.response;

import java.util.List;

import com.sdlc.vigor.entity.Role;


public class ResponseListRole implements Response {
	private int status;
	private String message;
	private List<Role> data;
	
	
	public ResponseListRole() {
	}
	
	public ResponseListRole(int status, String message, List<Role> data) {
		this.status = status;
		this.message = message;
		this.data = data;
	}
	
	public ResponseListRole(int status, String message) {
		this.status = status;
		this.message = message;
	}


	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Role> getData() {
		return data;
	}

	public void setData(List<Role> data) {
		this.data = data;
	}
}
