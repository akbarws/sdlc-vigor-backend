package com.sdlc.vigor.repository;

import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.sdlc.vigor.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Integer>{

	@Query(value = "Select * from role where name=?1",nativeQuery = true)
	Role findByName(String name);
	
	@Query(value = "Select * from role where id=?1",nativeQuery = true)
	Role findById(int id);
	
	@Transactional
	@Modifying
	@Query(value = "Update role set name = ?2 where id = ?1", nativeQuery = true)
	void editRole(int id, String name);
	
	@Transactional
	@Modifying
	@Query(value = "Update role set deleted_at = ?2 where id = ?1", nativeQuery = true)
	void deactiveRole(int id, Timestamp deleted_at);
	
	@Query(value = "Select * from role where name != 'ROLE_USER' and name != 'ROLE_ADMIN'",nativeQuery = true)
	List<Role> getRoleExceptRoleUser();
	
	
}
