package com.sdlc.vigor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.sdlc.vigor.entity.TransactionType;

public interface TransactionTypeRepository extends JpaRepository<TransactionType, Integer> {
	@Query(value = "Select * from transaction_type where id=?1",nativeQuery = true)
	TransactionType findById(int id);
	
}
