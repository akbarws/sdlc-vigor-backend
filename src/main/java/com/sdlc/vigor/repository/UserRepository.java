package com.sdlc.vigor.repository;

import java.sql.Timestamp;
import java.util.List;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.sdlc.vigor.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {
	
	@Query(value = "Select * from user where name=?1",nativeQuery = true)
	User findByUsername(String username);
	
	@Query(value = "Select * from user where id=?1",nativeQuery = true)
	User findById(int id);
	
	@Query(value = "Select * from user where email=?1",nativeQuery = true)
	User findByEmail(String email);
	
	@Transactional
	@Modifying
	@Query(value = "Update user set name = ?2, email =?3, phone=?4, address=?5, password = ?6 where id = ?1", nativeQuery = true)
	void updateUser(int id, String name, String email, String phone, String address, String password);
	
	@Transactional
	@Modifying
	@Query(value = "Update user set deleted_at = ?2 where id = ?1", nativeQuery = true)
	void deactiveUser(int id, Timestamp deleted_at);

	@Query(value = "Select * from user where role_id !=1 and role_id !=2", nativeQuery = true)
	List<User> findAllByRole();
}