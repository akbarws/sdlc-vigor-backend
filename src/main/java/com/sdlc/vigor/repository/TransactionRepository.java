package com.sdlc.vigor.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.sdlc.vigor.entity.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Integer> {
	@Query(value = "Select * from transaction where tanggal_booking like %:tanggal_booking%", nativeQuery = true)
	Optional<List<Transaction>> findByDate(String tanggal_booking);
	
	@Query(value = "Select * from transaction where user_id = ?1", nativeQuery = true)
	List<Transaction> findByUser(int user_id);
	

	@Query(value = "Select * from transaction where invoice_url is null", nativeQuery = true)
	List<Transaction> findByInvoice();

	@Query(value = "Select * from transaction where id=?1",nativeQuery = true)
	Transaction findById(int id);
	
	@Query(value = "Select * from transaction where status = 0", nativeQuery = true)
	List<Transaction> findByTrBelumLunas();
	
	@Query(value = "Select * from transaction where user_id =?1, desk_id =?2", nativeQuery = true)
	Transaction findByUserIDandDeskID(int user_id, int desk_id);
	
	@Query(value = "SELECT * FROM transaction WHERE desk_id = ?1", nativeQuery = true)
	Optional<Transaction> findByDeskId(int desk_id);
	
	@Query(value = "SELECT * FROM transaction WHERE user_id = ?1", nativeQuery = true)
	Optional<List<Transaction>> findByUserId(int userId);
	
	@Query(value = "Select * from transaction where tanggal_booking like ?1 and jam_booking like ?2 and desk_id = ?3 and status = 0", nativeQuery = true)
	Transaction findByTanggalJamDeskStatus(String tanggal_booking, String jam_booking, int desk_id);
	
	
}