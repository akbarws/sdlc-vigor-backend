package com.sdlc.vigor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.sdlc.vigor.entity.ShipmentStatus;

public interface ShipmentStatusRepository extends JpaRepository<ShipmentStatus, Integer> {
	
	@Query(value = "Select * from shipment_status where id=?1",nativeQuery = true)
	ShipmentStatus findById(int id);
	
}
