package com.sdlc.vigor.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.sdlc.vigor.entity.Desk;

public interface DeskRepository extends JpaRepository<Desk, Integer> {
	@Query(value = "SELECT * FROM desk WHERE desk.code = ?1", nativeQuery = true)
	Optional<Desk> findByCode(String code);
	
	@Query(value = "SELECT * FROM desk WHERE desk.code = ?1", nativeQuery = true)
	Desk findByCodee(String code);
	
	@Query(value = "SELECT * FROM desk WHERE id = ?1", nativeQuery = true)
	Desk findByID(int id);
	
	@Query(value = "SELECT * FROM desk WHERE desk.deleted_at is null AND number_of_people >= ?1 AND status = 1 ORDER BY number_of_people ASC", nativeQuery = true)
	Optional<List<Desk>> findByNop(int nop);
	
	@Query(value = "SELECT MAX(number_of_people) FROM desk WHERE desk.deleted_at is null", nativeQuery = true)
	int getMaxNop();
	
	@Query(value = "SELECT desk.* FROM desk WHERE desk.number_of_people >= 1 AND desk.status = 1 AND desk.id "
			+ "NOT IN(SELECT transaction.desk_id FROM transaction "
			+ "WHERE transaction.jam_booking = '13:00:00' AND transaction.tanggal_booking = '2021-11-24') "
			+ "ORDER BY desk.number_of_people ASC", nativeQuery = true)
	Optional<List<Desk>> findForTransaction(int nop, String tb, String jb);
	
	
	@Query(value = "SELECT * FROM desk WHERE number_of_people >= ?1 and status = 1", nativeQuery = true)
	List<Desk> findByNumberOfPeople(int numberOfPeople);	
	
	@Transactional
	@Modifying
	@Query(value = "Update desk set status_name = 'ready', status = 1 where id = ?1", nativeQuery = true)
	void updateStatusAvailable(int id);
	
	@Transactional
	@Modifying
	@Query(value = "Update desk set status_name ='booking' ,status = 0 where id = ?1", nativeQuery = true)
	void updateStatusToBooking(int id);
	
	@Transactional
	@Modifying
	@Query(value = "Update desk set status_name ='maintained' ,status = 0 where id = ?1", nativeQuery = true)
	void updateStatusToMaintained(int id);
	
}	