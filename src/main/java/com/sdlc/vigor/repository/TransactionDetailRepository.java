package com.sdlc.vigor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.sdlc.vigor.entity.Transaction;
import com.sdlc.vigor.entity.TransactionDetail;

public interface TransactionDetailRepository extends JpaRepository<TransactionDetail, Integer> {

	@Query(value = "Select * from transaction_detail where transaction_id=?1",nativeQuery = true)
	List<TransactionDetail> findByIdTr(int id);
	
}
