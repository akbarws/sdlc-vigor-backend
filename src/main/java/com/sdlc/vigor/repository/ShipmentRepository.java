package com.sdlc.vigor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.sdlc.vigor.entity.Shipment;

public interface ShipmentRepository extends JpaRepository<Shipment, Integer> {

	@Query(value = "Select * from shipment where transaction_id=?1",nativeQuery = true)
	Shipment findByTrId(int transaction_id);
	
}
