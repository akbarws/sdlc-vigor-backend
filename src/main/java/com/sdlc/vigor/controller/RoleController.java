package com.sdlc.vigor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sdlc.vigor.entity.Role;
import com.sdlc.vigor.response.ResponseListRole;
import com.sdlc.vigor.response.ResponseObject;
import com.sdlc.vigor.service.RoleService;

@RestController
@RequestMapping("/api")
public class RoleController {
	
	@Autowired
	private RoleService service;

	@PostMapping("/role")
	public ResponseEntity<ResponseObject> addRole(@RequestBody Role role) {
		return service.addRole(role);
	}

	@GetMapping("/role/{id}")
	public ResponseEntity<ResponseObject> findById(@PathVariable int id) {
		return service.findById(id);
	}
	
	@GetMapping(value = "/roles", produces = "application/json")
	public ResponseEntity<ResponseListRole> getAllRole() {
		return service.getAllRole();
	}
	
	@PutMapping(value = "/role/{id}", produces = "application/json", consumes = "application/json")
	public ResponseEntity<ResponseObject> editRole(@PathVariable int id, @RequestBody Role payload) {
		return service.editRole(id, payload);
	}
	
	@PutMapping(value = "/role/deactive/{id}")
	public ResponseEntity<ResponseObject> deactiveRole(@PathVariable int id) {
		return service.deactiveRole(id);
	}
	
	@GetMapping(value = "/role", produces = "application/json")
	public ResponseEntity<ResponseObject> getRoleExceptRoleUser() {
		return ResponseEntity.ok(service.getRoleExceptRoleUser());
	}
	
}
