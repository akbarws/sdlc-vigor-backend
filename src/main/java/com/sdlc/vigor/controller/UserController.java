package com.sdlc.vigor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sdlc.vigor.entity.User;
import com.sdlc.vigor.response.ResponseObject;
import com.sdlc.vigor.response.ResponseListUser;
import com.sdlc.vigor.service.UserService;

@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	private UserService service;

	@PostMapping("/public/register")
	public ResponseEntity<ResponseObject> register(@RequestBody User user, Errors errors) {
		if (errors.hasErrors()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Data invalid"));
		}
		return service.save(user);
	}

	@GetMapping("/user/{id}")
	public ResponseEntity<ResponseObject> findById(@PathVariable int id) {
		return service.findById(id);
	}

	@PostMapping(value = "/public/login")
	public ResponseEntity<ResponseObject> createAuthenticationToken(@RequestBody User user) throws Exception {
		return service.createAuthenticationToken(user);
	}

	@GetMapping(value = "/users", produces = "application/json")
	public ResponseEntity<ResponseListUser> getAllUser() {
		return service.getAllUser();
	}
	
	@PutMapping(value = "/user/{id}", produces = "application/json", consumes = "application/json")
	public ResponseEntity<ResponseObject> updateUser(@PathVariable int id, @RequestBody User payload) {
		return service.updateUser(id, payload);
	}
	
	@PutMapping(value = "/user/deactive/{id}")
	public ResponseEntity<ResponseObject> deactiveUser(@PathVariable int id) {
		return service.deactiveUser(id);
	}
	
	@GetMapping(value = "/users/except", produces = "application/json")
	public ResponseEntity<ResponseListUser> getUserExceptCustomerAdmin() {
		return service.getUserExcept();
	}
	
	@PostMapping("/admin/addEmployee/{idRole}")
	public ResponseEntity<ResponseObject> addEmployee(@PathVariable int idRole, @RequestBody User user) {
		return service.addEmployee(idRole, user);
	}
}
