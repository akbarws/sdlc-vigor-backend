package com.sdlc.vigor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sdlc.vigor.entity.Desk;
import com.sdlc.vigor.response.ResponseObject;
import com.sdlc.vigor.service.DeskService;

@RestController
@RequestMapping("/api")
public class DeskController {

	@Autowired
	DeskService service;

	@GetMapping(value = "/desks", produces = "application/json")
	public ResponseEntity<ResponseObject> getAllDesks() {
		return ResponseEntity.ok(service.getAll());
	}
	
	@GetMapping(value = "/desks/active", produces = "application/json")
	public ResponseEntity<ResponseObject> getActive() {
		return ResponseEntity.ok(service.getAllActive());
	}
	
	@GetMapping(value = "/desk/maxnop", produces = "application/json")
	public ResponseEntity<ResponseObject> getmaxNop() {
		return ResponseEntity.ok(service.getMaxNop());
	}
	
	@GetMapping(value = "/desks/active/{numberOfPeople}", produces = "application/json")
	public ResponseEntity<ResponseObject> getActiveByNop(@PathVariable String numberOfPeople) {
		return ResponseEntity.ok(service.getActiveByNop(numberOfPeople));
	}
	
	@GetMapping(value = "/desk/{id}", produces = "application/json")
	public ResponseEntity<ResponseObject> get(@PathVariable String id) {
		return ResponseEntity.ok(service.getDeskById(id));
	}
	
	@PostMapping(value = "/desk", consumes = "application/json", produces = "application/json")
	public ResponseEntity<ResponseObject> add(@RequestBody Desk desk) {
		return ResponseEntity.ok(service.add(desk));
	}
	
//	@PostMapping(value = "/desk", consumes = "multipart/form-data", produces = "application/json")
//	public ResponseEntity<ResponseObject> add(@ModelAttribute Desk desk, @RequestParam("image") MultipartFile image) {
//		return ResponseEntity.ok(service.add(desk, image));
//    }

	@GetMapping(value = "/desks/transaction", produces = "application/json")
	public ResponseEntity<ResponseObject> getAvailableForTransaction(
			@RequestParam(name = "nop", required = true) String nop,
			@RequestParam(name = "tb", required = true) String tb,
			@RequestParam(name = "jb", required = true) String jb
			) {
		return ResponseEntity.ok(service.getAvailableForTr(nop, tb, jb));
	}
	
	@PutMapping(value = "/desk/{id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<ResponseObject> edit(@PathVariable String id, @RequestBody Desk desk) {
		return ResponseEntity.ok(service.update(id, desk));
	}
	
	@DeleteMapping(value = "/desk/{id}", produces = "application/json")
	public ResponseEntity<ResponseObject> delete(@PathVariable String id) {
		return ResponseEntity.ok(service.delete(id));
	}
	
	@GetMapping(value = "/desk/filterByPeople/{people}", produces = "application/json")
	public ResponseEntity<ResponseObject> findByNumberOfPeople(@PathVariable String people) {
		return ResponseEntity.ok(service.findByNumberOfPeople(people));
	}
	
	@PutMapping(value = "/desk/status/{id}", produces = "application/json", consumes = "application/json")
	public ResponseEntity<ResponseObject> updateStatusAvailable(@PathVariable int id) {
		return service.updateStatusAvailable(id);
	}
	
	@PutMapping(value = "/desk/status/book/{id}", produces = "application/json", consumes = "application/json")
	public ResponseEntity<ResponseObject> updateStatusToBooking(@PathVariable int id) {
		return service.updateStatusToBooking(id);
	}
	
	@PutMapping(value = "/desk/status/maintain/{id}", produces = "application/json", consumes = "application/json")
	public ResponseEntity<ResponseObject> updateStatusToMaintain(@PathVariable int id) {
		return service.updateStatusToMaintained(id);
	}
}