package com.sdlc.vigor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sdlc.vigor.response.ResponseObject;
import com.sdlc.vigor.service.ShipmentService;

@RestController
@RequestMapping("/api")
public class ShipmentController {

	@Autowired
	ShipmentService service;
	
	@GetMapping(value = "/shpment/getByTrId/{id}", produces = "application/json")
	public ResponseEntity<ResponseObject> getByTrId(@PathVariable String id){
		return ResponseEntity.ok(service.getByTrId(id));
	}
	
	@PostMapping(value = "/shpment/{idTr}/{idShipmentStatus}/{idUser}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<ResponseObject> add(@PathVariable int idTr, @PathVariable int idShipmentStatus, @PathVariable int idUser){
		return ResponseEntity.ok(service.add(idTr, idShipmentStatus, idUser));
	}
}