package com.sdlc.vigor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.sdlc.vigor.entity.Packet;
import com.sdlc.vigor.response.ResponseObject;
import com.sdlc.vigor.service.PacketService;

@RestController
@RequestMapping("/api")
public class PacketController {

	@Autowired
	PacketService service;
	
	@GetMapping(value = "/packets", produces = "application/json")
	public ResponseEntity<ResponseObject> getall(){
		return ResponseEntity.ok(service.getAll());
	}
	
	@GetMapping(value = "/public/packets/active", produces = "application/json")
	public ResponseEntity<ResponseObject> getallACtive(){
		return ResponseEntity.ok(service.getAllActive());
	}
	
	@GetMapping(value = "/public/packet/{id}", produces = "application/json")
	public ResponseEntity<ResponseObject> getById(@PathVariable String id) {
		return ResponseEntity.ok(service.getById(id));
	}
	
	@PostMapping(value = "/packet", consumes = "multipart/form-data", produces = "application/json")
	public ResponseEntity<ResponseObject> add(@ModelAttribute Packet newPacket, @RequestParam("image") MultipartFile image) {
		return ResponseEntity.ok(service.add(newPacket, image));
	}
	
	@PutMapping(value = "/packet/{id}", consumes = "multipart/form-data", produces = "application/json") 
	public ResponseEntity<ResponseObject> edit(@PathVariable String id, @ModelAttribute Packet newPacket, MultipartFile image){
		return ResponseEntity.ok(service.updateById(id, newPacket, image));
	}
	
	@DeleteMapping(value = "/packet/{id}", produces = "application/json")
	public ResponseEntity<ResponseObject> delete(@PathVariable String id){
		return ResponseEntity.ok(service.deleteById(id));
	}
	
	@GetMapping(value = "/public/packet/getTreePacket", produces = "application/json")
	public ResponseEntity<ResponseObject> getTreePacket(){
		return ResponseEntity.ok(service.getTreePacket());
	}
}