package com.sdlc.vigor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sdlc.vigor.entity.TransactionType;
import com.sdlc.vigor.response.ResponseObject;
import com.sdlc.vigor.service.TransactionTypeService;

@RestController
@RequestMapping("/api")
public class TransactionTypeController {

	@Autowired
	TransactionTypeService service;
	
	@GetMapping(value = "/transtypes", produces = "application/json")
	public ResponseEntity<ResponseObject> getAll() {
		return ResponseEntity.ok(service.getAll());
	}
	
	@GetMapping(value = "/transtypes/active", produces = "application/json")
	public ResponseEntity<ResponseObject> getAllActive() {
		return ResponseEntity.ok(service.getAllActive());
	}
	
	@GetMapping(value = "/transtype/{id}", produces = "application/json")
	public ResponseEntity<ResponseObject> getById(@PathVariable String id) {
		return ResponseEntity.ok(service.getById(id));
	}
	
	@PostMapping(value = "/transtype", consumes = "application/json", produces = "application/json")
	public ResponseEntity<ResponseObject> add(@RequestBody TransactionType transactionType) {
		return ResponseEntity.ok(service.add(transactionType));
	}
	
	@PutMapping(value = "/transtype/{id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<ResponseObject> edit(@PathVariable String id, @RequestBody TransactionType transactionType){
		return ResponseEntity.ok(service.edit(id, transactionType));
	}
	
	@DeleteMapping(value = "/transatype/{id}", produces = "application/json")
	public ResponseEntity<ResponseObject> delete(@PathVariable String id){
		return ResponseEntity.ok(service.delete(id));
	}
}
