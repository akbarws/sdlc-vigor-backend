package com.sdlc.vigor.controller;

import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sdlc.vigor.entity.Transaction;
import com.sdlc.vigor.response.ResponseObject;
import com.sdlc.vigor.service.TransactionService;
import com.midtrans.Config;
import com.midtrans.ConfigFactory;
import com.midtrans.httpclient.error.MidtransError;
import com.midtrans.service.MidtransCoreApi;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

@RestController
@RequestMapping("/api")
public class TransactionController {

	@Autowired
	TransactionService service;

	@GetMapping(value = "/transactions", produces = "application/json")
	public ResponseEntity<ResponseObject> getall() {
		return ResponseEntity.ok(service.getAll());
	}

	@GetMapping(value = "/transaction/{id}", produces = "application/json")
	public ResponseEntity<ResponseObject> getById(@PathVariable String id) {
		return ResponseEntity.ok(service.getById(id));
	}

	// ad tr user online
	@PostMapping(value = "/transaction/online/{idU}/{idTrType}/{idDesk}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<ResponseObject> addTrUserOnline(@PathVariable int idU, @PathVariable int idTrType,
			@PathVariable int idDesk, @RequestBody Transaction tr) {
		return ResponseEntity.ok(service.addTrUserOnline(idU, idTrType, idDesk, tr));
	}

	// ad tr user offline dr waiter
	@PostMapping(value = "/transaction/offline/{idU}/{idTrType}/{idDesk}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<ResponseObject> addTrUserOffline(@PathVariable int idU, @PathVariable int idTrType,
			@PathVariable int idDesk, @RequestBody Transaction tr) {
		return ResponseEntity.ok(service.addTrUserOffline(idU, idTrType, idDesk, tr));
	}

	// ad tr user delivery (tanpa meja)
	@PostMapping(value = "/transaction/{idU}/{idTrType}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<ResponseObject> addTrUserDelivery(@PathVariable int idU, @PathVariable int idTrType,
			@RequestBody Transaction tr) {
		return ResponseEntity.ok(service.addTrUserDelivery(idU, idTrType, tr));
	}

	// ad tr user online + TEMEN
	@PostMapping(value = "/transaction/tambahan/{idTrTemen}/{idU}/{idTrType}/{codeDesk}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<ResponseObject> addTrUserOnlineNambahTemen(@PathVariable int idTrTemen, @PathVariable int idU,
			@PathVariable int idTrType, @PathVariable String codeDesk, @RequestBody Transaction tr) {
		return ResponseEntity.ok(service.addTrUserOnlineNambahTemen(idTrTemen, idU, idTrType, codeDesk, tr));
	}

	@PutMapping(value = "/transaction/updateTable/{id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<ResponseObject> updateTable(@PathVariable String id, @RequestBody String deskId) {
		return ResponseEntity.ok(service.updateTable(id, deskId));
	}

	@GetMapping(value = "/transaction/getDateAndUser/{deskCode}/{jam_booking}")
	public ResponseEntity<ResponseObject> getTableByDateAndUserNameAndBelumLunas(@PathVariable String deskCode,
			@PathVariable String jam_booking) {
		return ResponseEntity.ok(service.getTableByDateAndUserNameAndBelumLunas(deskCode, jam_booking));
	}

	// update table penalty
	@PutMapping(value = "/transaction/updateTablePenalty/{id}")
	public ResponseEntity<ResponseObject> updateTableByPenalty(@PathVariable int id, @RequestBody Transaction payload) {
		return ResponseEntity.ok(service.updateTableByPenalty(id, payload));
	}

	@GetMapping(value = "/transaction/user/{id}", produces = "application/json")
	public ResponseEntity<ResponseObject> getTransactionByUserId(@PathVariable String id) {
		System.out.println(id);
		return ResponseEntity.ok(service.getByUserId(id));
	}

	@PostMapping(value = "/notification", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> handleNotification(@RequestBody Map<String, Object> response)
			throws MidtransError, JSONException {
		String notifResponse = null;

		MidtransCoreApi coreApi = new ConfigFactory(
				new Config("SB-Mid-server-d4jUiLxuoSa4Uap8oXV8lL2R", "SB-Mid-client-TcXnB89obum6wM0g", false))
						.getCoreApi();
		if (!(response.isEmpty())) {
			String orderId = (String) response.get("order_id");
			JSONObject transactionResult = coreApi.checkTransaction(orderId);

			String transactionStatus = (String) transactionResult.get("transaction_status");
			String fraudStatus = (String) transactionResult.get("fraud_status");

			notifResponse = "Transaction notification received. Order ID: " + orderId + ". Transaction status: "
					+ transactionStatus + ". Fraud status: " + fraudStatus;
			System.out.println(notifResponse);
			switch (transactionStatus) {
			case "settlement":
				// noinspection StatementWithEmptyBody
				if (fraudStatus.equals("challenge")) {
					// TODO set transaction status on your database to 'challenge' e.g: 'Payment
					// status challenged. Please take action on your Merchant Administration Portal
				} else if (fraudStatus.equals("accept")) {
					System.out.println("BEFORE CALL ORDER SERVICE");
				}
				break;
			case "cancel":
				// TODO set transaction status on your database to 'cancel'
				break;
			case "deny":
				// TODO set transaction status on your database to 'deny'
				break;
			case "expire":
				// TODO set transaction status on your database to 'failure'
				break;
			case "pending":
				// TODO set transaction status on your database to 'pending' / waiting payment
				break;
			default:
				throw new IllegalStateException("Unexpected value: " + transactionStatus);
			}
		}
		return new ResponseEntity<>(notifResponse, HttpStatus.OK);
	}
}
