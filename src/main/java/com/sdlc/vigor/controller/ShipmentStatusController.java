package com.sdlc.vigor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sdlc.vigor.entity.ShipmentStatus;
import com.sdlc.vigor.response.ResponseObject;
import com.sdlc.vigor.service.ShipmentStatusService;

@RestController
@RequestMapping("/api")
public class ShipmentStatusController {

	@Autowired
	ShipmentStatusService service;
	
	@GetMapping(value = "/shipments", produces = "application/json")
	public ResponseEntity<ResponseObject> getAll(){
		return ResponseEntity.ok(service.getAll());
	}
	
	@GetMapping(value = "/shipments/active", produces = "application/json")
	public ResponseEntity<ResponseObject> getAllActive(){
		return ResponseEntity.ok(service.getAllActive());
	}
	
	@GetMapping(value = "/shipment/{id}", produces = "application/json")
	public ResponseEntity<ResponseObject> getById(@PathVariable String id){
		return ResponseEntity.ok(service.getById(id));
	}
	
	@PostMapping(value = "/shipment", consumes = "application/json", produces = "application/json")
	public ResponseEntity<ResponseObject> add(@RequestBody ShipmentStatus shipmentstatus){
		return ResponseEntity.ok(service.add(shipmentstatus));
	}
	
	@PutMapping(value = "/shipment/{id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<ResponseObject> edit(@PathVariable String id, @RequestBody ShipmentStatus shipmentstatus){
		return ResponseEntity.ok(service.edit(id, shipmentstatus));
	}
	
	@DeleteMapping(value = "/shipment/{id}", produces = "application/json")
	public ResponseEntity<ResponseObject> deleteById(@PathVariable String id){
		return ResponseEntity.ok(service.delete(id));
	}
	
}
