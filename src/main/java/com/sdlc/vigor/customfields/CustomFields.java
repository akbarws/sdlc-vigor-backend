package com.sdlc.vigor.customfields;

public interface CustomFields {
	public interface getAllUser{
		public int getId();
		public String getName();
		public String getEmail();
	}
}
