package com.sdlc.vigor.entity;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

@Entity
public class Transaction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;
	@OneToOne
	@JoinColumn(name = "transaction_type_id", referencedColumnName = "id")
	private TransactionType transactionType;
	@OneToOne
	@JoinColumn(name = "desk_id", referencedColumnName = "id")
	private Desk desk;
	
	private float grandTotal;
	private boolean status;
	private String invoiceUrl;
	
	private float overtime;
	private float leftover;
	private float totalPenalty;
	private String paymentToken;
	private String orderId;
	
//	@JsonIgnore
	@OneToMany(mappedBy = "transaction", fetch = FetchType.EAGER)
	private List<TransactionDetail> transactionDetails;
	@CreationTimestamp
	private Timestamp created_at;
	
	private String tanggalBooking;
	private String jamBooking;
	
	public Transaction() {}

	public Transaction(User user, TransactionType transactionType, Desk desk, float grandTotal, boolean status,
			String invoiceUrl, float overtime, float leftover, float totalPenalty, String tanggalBooking,
			String jamBooking) {
		this.user = user;
		this.transactionType = transactionType;
		this.desk = desk;
		this.grandTotal = grandTotal;
		this.status = status;
		this.invoiceUrl = invoiceUrl;
		this.overtime = overtime;
		this.leftover = leftover;
		this.totalPenalty = totalPenalty;
		this.tanggalBooking = tanggalBooking;
		this.jamBooking = jamBooking;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public Desk getDesk() {
		return desk;
	}

	public void setDesk(Desk desk) {
		this.desk = desk;
	}

	public float getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(float grandTotal) {
		this.grandTotal = grandTotal;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getInvoiceUrl() {
		return invoiceUrl;
	}

	public void setInvoiceUrl(String invoiceUrl) {
		this.invoiceUrl = invoiceUrl;
	}

	public float getOvertime() {
		return overtime;
	}

	public void setOvertime(float overtime) {
		this.overtime = overtime;
	}

	public float getLeftover() {
		return leftover;
	}

	public void setLeftover(float leftover) {
		this.leftover = leftover;
	}

	public float getTotalPenalty() {
		return totalPenalty;
	}

	public void setTotalPenalty(float totalPenalty) {
		this.totalPenalty = totalPenalty;
	}

	public Timestamp getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

	public List<TransactionDetail> getTransactionDetails() {
		return transactionDetails;
	}

	public void setTransactionDetails(List<TransactionDetail> transactionDetails) {
		this.transactionDetails = transactionDetails;
	}

	public String getTanggalBooking() {
		return tanggalBooking;
	}

	public void setTanggalBooking(String tanggalBooking) {
		this.tanggalBooking = tanggalBooking;
	}

	public String getJamBooking() {
		return jamBooking;
	}

	public void setJamBooking(String jamBooking) {
		this.jamBooking = jamBooking;
	}

	public String getPaymentToken() {
		return paymentToken;
	}

	public void setPaymentToken(String paymentToken) {
		this.paymentToken = paymentToken;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	
}
