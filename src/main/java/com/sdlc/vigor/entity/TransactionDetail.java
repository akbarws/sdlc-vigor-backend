package com.sdlc.vigor.entity;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class TransactionDetail {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "transaction_id", referencedColumnName = "id")
	private Transaction transaction;
	@OneToOne
	@JoinColumn(name = "packet_id", referencedColumnName = "id")
	private Packet packet;
	private int qty;
	@CreationTimestamp
	private Timestamp created_at;
	
	public TransactionDetail() {}
	
	public TransactionDetail(Packet packet, int qty) {
		super();
		this.packet = packet;
		this.qty = qty;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public Packet getPacket() {
		return packet;
	}

	public void setPacket(Packet packet) {
		this.packet = packet;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	@Override
	public String toString() {
		return "TransactionDetail [id=" + id + ", transaction=" + transaction + ", packet=" + packet + ", qty=" + qty
				+ ", created_at=" + created_at + "]";
	}
}
