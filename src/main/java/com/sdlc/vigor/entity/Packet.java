package com.sdlc.vigor.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

@Entity
public class Packet {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(nullable = false)
	private String name;
	@Column(nullable = false)
	private int price;
	@Column(nullable = false)
	private String img_url;
	@CreationTimestamp
	private Timestamp created_at;
	@Column(nullable = true)
	private Timestamp deleted_at;
	private String description;
	
	
	public Packet() {}
	
	public Packet(String name, int price) {
		super();
		this.name = name;
		this.price = price;
	}
	
	public Packet(String name, int price, String description, String img_url) {
		super();
		this.name = name;
		this.price = price;
		this.description = description;
		this.img_url = img_url;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getImg_url() {
		return img_url;
	}

	public void setImg_url(String img_url) {
		this.img_url = img_url;
	}
	
	public Timestamp getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(Timestamp deleted_at) {
		this.deleted_at = deleted_at;
	}

	public Timestamp getCreated_at() {
		return created_at;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

	@Override
	public String toString() {
		return "Packet [id=" + id + ", name=" + name + ", price=" + price + ", img_url=" + img_url + ", created_at="
				+ created_at + ", deleted_at=" + deleted_at + ", description=" + description + "]";
	}
	
}
