package com.sdlc.vigor.entity;

public class UserLogin {
	private int id;
	private String name, role, token;
	
	public UserLogin(int id, String name, String role, String token) {
		super();
		this.id = id;
		this.name = name;
		this.role = role;
		this.token = token;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
